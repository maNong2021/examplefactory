package com.example.factory;

import com.example.factory.intf.CpuFactory;
import com.example.factory.intf.impl.CpuAFactory;
import com.example.product.intf.Cpu;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description:
 */
public class TestFactoryMethod {
    public  static void main( String[] args ){
        CpuFactory cpuFactory = new CpuAFactory();
        Cpu cpuA = cpuFactory.createCpu();
        cpuA.getCpuName();
    }
}
