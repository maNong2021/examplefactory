package com.example.product.intf;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description:
 */
public interface Cpu {
    String getCpuName();
}
