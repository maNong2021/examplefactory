package com.example.product.intf.impl;

import com.example.product.intf.Cpu;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description:
 */
public class CpuB implements Cpu {
    @Override
    public String getCpuName() {
        return " cpu B ";
    }

}
