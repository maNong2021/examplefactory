package com.example.factory.intf;

import com.example.product.intf.Cpu;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description:
 */
public interface CpuFactory {

    Cpu createCpu();
}
