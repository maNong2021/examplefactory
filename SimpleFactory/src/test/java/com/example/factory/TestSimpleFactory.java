package com.example.factory;

import com.example.intf.Cpu;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description:
 */
public class TestSimpleFactory {

    public static void main( String[] args ){
        SimpleFactory simpleFactory = new SimpleFactory();
        Cpu cpu = simpleFactory.createCpu("CpuA");
    }
}
