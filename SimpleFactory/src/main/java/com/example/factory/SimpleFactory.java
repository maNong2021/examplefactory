package com.example.factory;

import com.example.intf.Cpu;
import com.example.intf.impl.CpuA;
import com.example.intf.impl.CpuB;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description: 简单工厂模式
 */
public class SimpleFactory {

    public Cpu createCpu(String productName){
        if ( productName.equals( CpuA.class.getSimpleName() ) ){
            return new CpuA();
        }else if ( productName.equals( CpuB.class.getSimpleName() ) ){
            return new CpuB();
        }

        return null;
    }
}
