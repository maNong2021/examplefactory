package com.example.intf.impl;

import com.example.intf.Cpu;

/**
 * @author: chenyuanzhu
 * @date: 2021/7/8
 * @description:
 */
public class CpuA implements Cpu {
    @Override
    public String getCpuName() {
        return " cpu A ";
    }
}
